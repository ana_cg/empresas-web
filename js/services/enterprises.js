services.factory('EnterpriseService', function($q, $http) {

	var req = {};
	var API = 'http://54.94.179.135:8090/api/v1/enterprises'

	req.getList = () => {
	  var deferred = $q.defer();

	  $http({
	    method: 'GET',
	    url: API,
	    headers: {
	       'Content-Type': 'application/json',
           'access-token': window.localStorage.getItem('usertoken'),
           'client': window.localStorage.getItem('client'),
           'uid': window.localStorage.getItem('uid')
         }
	  }).then(deferred.resolve, deferred.reject);

	  return deferred.promise
	}

	req.getDetail = (itemID) => {
	  var deferred = $q.defer();

	  $http({
	    method: 'GET',
	    url: API + '/' + itemID,
	    headers: {
	       'Content-Type': 'application/json',
           'access-token': window.localStorage.getItem('usertoken'),
           'client': window.localStorage.getItem('client'),
           'uid': window.localStorage.getItem('uid')
         }
	  }).then(deferred.resolve, deferred.reject);

	  return deferred.promise
	}

	return req;
});