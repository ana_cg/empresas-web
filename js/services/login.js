services.factory('LoginService', function($q, $http) {

	var req = {};
	var API = 'http://54.94.179.135:8090/api/v1/users/auth/sign_in'

	req.requestToken = (user) => {
	  var deferred = $q.defer();

	  $http({
	    method: 'POST',
	    url: API,
		data: user
	  }).then(deferred.resolve, deferred.reject);

	  return deferred.promise
	}

	return req;
});