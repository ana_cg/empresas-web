var app = angular.module('app',['services', 'controllers', 'ui.router', 'ngMessages']);

var services = angular.module('services', []);
var controllers = angular.module('controllers', []);

app.config(function($stateProvider, $urlRouterProvider){

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('login', {
      url: '/',
      templateUrl: 'index.html',
      controller: 'LoginController'
    })

    .state('search', {
      url: '/search',
      abstract: true,
      templateUrl: 'views/search.html'
    })

    .state('search.list', {
      url: '/list',
      templateUrl: 'views/enterprise-list.html',
      controller: 'EnterpriseController'
    })

    .state('search.detail', {
      url: '/list/:enterpriseID',
      templateUrl: 'views/enterprise-detail.html',
      controller: 'EnterpriseDetailController'
    });
});