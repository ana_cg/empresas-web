controllers.controller('EnterpriseDetailController', function($scope, $state, $timeout, $stateParams, EnterpriseService) {

  /* scope variables */
  $scope.showSearch = false;
  $scope.modalOpen = false;

  /* scope functions */
  $scope.getEnterprise = (itemID) => {
      EnterpriseService.getDetail(itemID).then(
        (response) => {
          console.log(response.data);
          console.log('detail: 200OK');

          $scope.enterprise =  response.data.enterprise;
        }, (error) => {
              console.log(error);
              if(error.status === 401) {
                $state.go('login');
              }
          });
      }

    $scope.$watch('$viewContentLoaded', function() {
      $scope.getEnterprise($stateParams.enterpriseID);
  });

});