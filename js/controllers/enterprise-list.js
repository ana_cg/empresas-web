controllers.controller('EnterpriseController', function($scope, $timeout, $state, EnterpriseService) {

  /* scope variables */
  $scope.showSearch = false;

  /* scope functions */
  $scope.getEnterprises = () => {
    EnterpriseService.getList().then(
      (response) => {
        console.log('list: 200OK');
        $scope.enterprises =  response.data.enterprises;
      }, (error) => {
            console.log(error);
            if(error.status === 401) {
                $state.go('login')
              }
        });
    }

  $scope.openDetails = function(id){
    $state.go('search.detail', {'enterpriseID': id})
  };

   $scope.$watch('$viewContentLoaded', function() {
      $scope.getEnterprises();
  });

});