controllers.controller('LoginController', function($scope, $timeout, $http, $state, $location, LoginService) {

  $scope.notFound = false;

  $scope.getToken = () => {
      LoginService.requestToken($scope.user).then(
        (response) => {

          $scope.notFound = false;
          window.localStorage.setItem('usertoken', (response.headers()['access-token']));
          window.localStorage.setItem('client', (response.headers()['client']));
          window.localStorage.setItem('uid', (response.headers()['uid']));

          $state.go('search.list');

        }, (error) => {
              console.log(error);
              if(error.status === 401) {
                $scope.notFound = true;
              }
          });
      }

  $scope.$watch('$viewContentLoaded', function() {
    // window.localStorage.setItem('usertoken', '');
    // window.localStorage.setItem('client', '');
    // window.localStorage.setItem('uid', '');
  });

});