# README #

O projeto foi criado utilizando os frameworks AngularJS e Materialize.

### Rodando o projeto ###

Para rodar o projeto basta instalar as dependências

```
npm install
```

Instalar algum servidor como o http-server

```
npm install http-server -g
```

E por fim rodar o servidor

```
http-server
```
