var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');

gulp.task('style', function() {
    gulp.src('styles/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./styles/css/'))
});

/* main task */
gulp.task('default',function() {
    gulp.watch('styles/scss/**/*.scss',['style']);
});